wldhx/gitlab-ci-react-native-android
===========================================

.. image:: https://gitlab.com/quajoduke305/rn-android-ci-image/badges/master/build.svg
    :target: https://.gitlab.com/quajoduke305/rn-android-ci-image/commits/master
    :alt: Build status

This images is base off wldhx/gitlab-ci-android to allow building React Native applications for Android with node v14x. Similarly, built images are stored in GitLab Container Registry. 

Example ``.gitlab-ci.yml``
--------------------------

.. code:: yaml

    image: registry.gitlab.com/quajoduke305/rn-android-ci-image:master

    stages:
      - build

    before_script:
      - export GRADLE_USER_HOME=$(pwd)/.gradle
      - chmod +x ./android/gradlew

    cache:
      key: ${CI_PROJECT_ID}
      paths:
      - node_modules/
      - .gradle/

    build:
      stage: build
      script:
        - yarn install
        - cd android/
        - ./gradlew assembleDebug
      artifacts:
        paths:
          - android/app/build/outputs/apk/app-debug.apk
